const rest = JSON.parse(localStorage.restorationBD);
console.log(rest);

if (!Array.isArray(rest)) {
  throw Error("Something is wrong");
}

const restEl = rest.map(
  ({
    description,
    id,
    ingridients,
    keywords,
    price,
    productName,
    productWeigth,
    productimageUrl,
  }) => {
    return `
    <div class="video">
<h3>${productName}</h3>
<img src="${productimageUrl}" alt="${productName}" id="${id}">
<div>
<p>${description}</p>
  <p>Склад: ${
    ingridients.includes("string") ? "інформація відсутня" : `${ingridients}`
  }</p>
  <p>Вихід (вага): ${
    productWeigth.includes("number")
      ? "інформація відсутня"
      : `${productWeigth} `
  }</p>
  <p>Вартість: ${price}</p>
  <div>
    ${keywords
      .map((el) => {
        return `<span class="badge bg-secondary">${el}</span>`;
      })
      .join("")}
     </div>
    </div>
    `;
  }
);

document
  .querySelector(".rest-box")
  .insertAdjacentHTML("beforeend", restEl.join(""));
